// eventstore
// MQTT
// Apache Geode
// Hangfire
// https://tiddlywiki.com/
// NuKeeper
// NRules
// http://nibbler.silktide.com/en
// https://medium.com/lankapura/angular-asp-net-core-authentication-with-identity-server-4-65db6b9f7c2a
// TabCarousel for chrome
// https://github.com/SeleniumHQ/docker-selenium
// https://martinfowler.com/articles/practical-test-pyramid.html
// https://www.npmjs.com/package/swaggerbank
// Fluent assertions

// docker run --rm --publish=7474:7474 --publish=7687:7687 neo4j:3.0
// :server connect

// browser
// https://10-0-1-179-37013.neo4jsandbox.com/browser/

// CORS extension for style
// https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi

// yFiles
// https://www.yworks.com/blog/neo4j-node-design

// https://www.fromlatest.io/
// https://www.npmjs.com/package/dockerfilelint

// TODO:
// v ng and nc to git io gitlab
// v packages to io packages in
// - replace sass with css variables https://medium.com/@amcdnl/theming-angular-with-css-variables-3c78a5b20b24 + polyfill https://codepen.io/aaronbarker/pen/MeaRmL
// - reorganize security nodes
//
// https://blog.cloudboost.io/angular-jest-wallabyjs-why-it-is-the-ideal-combination-and-how-to-configure-b4cbe2eff4b3
//
// https://github.com/brandonroberts/ngrx-store-freeze
// ?[in dep io devdep]
//
// ?[npm needed in dep]
//
// deep freeze
//
// https://opensource.org/licenses
//
// http://www.ngx-translate.com/
//
// https://www.owasp.org/index.php/OWASP_Dependency_Check
//
// Balsamiq
// A ruler for Windows
// Golden ratio
// Cmdr
// Git bash
// Process Explorer
//
// HEROES
// ======
// Scott Hanselman
// Jimmy Bogard
// Martin Fowler
// Kwinten Pisman
// Brecht Billiet
// Udi Dahan
// Kurt Goossens
// Alan Cooper
// Jason Fried
// Kelsey Hightower
// Casey Neistat
// Jan Vercammen
// Jo Bossaerts
// Simon Brown
// John Papa
// Tim Ferris

============================

// style
:style https://gitlab.com/devgem/techradar/raw/master/technology-stack.grass

============================

// delete all nodes
MATCH (n)
DETACH DELETE n

=============================

// create all nodes
CREATE

// technologies
(dk:Technology { name: "Docker", link: "https://www.docker.com/" }),
(kb:Technology { name: "Kubernetes", link: "https://kubernetes.io/" }),
(do:Technology { name: "Digital Ocean", link: "https://www.digitalocean.com/", categories: "infra" }),
(gl:Technology { name: "GitLab", link: "https://gitlab.com/", categories: "infra, docu" }),
(git:Technology { name: "Git", link: "https://git-scm.com" }),
(github:Technology { name: "GitHub", link: "https://github.com/" }),
(dh:Technology { name: "Docker Hub", link: "https://hub.docker.com/" }),
(glr:Technology { name: "GitLab Runner", link: "https://docs.gitlab.com/runner/" }),
(ga:Technology { name: "Google Analytics", link: "https://www.google.com/intl/nl/analytics/", categories: "marketing, monitoring" }),
(se:Technology { name: "Sentry", link: "https://sentry.io/", categories: "logging, monitoring" }),
(lda:Technology { name: "LogDNA", link: "https://logdna.com/", categories: "logging" }),
(appi:Technology { name: "Application Insights", link: "https://azure.microsoft.com/en-us/services/application-insights/", categories: "logging, monitoring" }),
(insp:Technology { name: "Inspectlet", link: "https://www.inspectlet.com/", categories: "marketing, monitoring" }),
(sq:Technology { name: "Sonar Qube", link: "https://www.sonarqube.org/", categories: "qa" }),
(id4:Technology { name: "Identity Server 4", link: "http://identityserver.io/", categories: "secu" }),
(ruri:Technology { name: "Report URI", link: "https://report-uri.com/", categories: "secu" }),
(cf:Technology { name: "Cloudflare", link: "https://www.cloudflare.com/", categories: "secu" }),
(ngun:Technology { name: "Angular Universal", link: "https://universal.angular.io/" }),
(mbg:Technology { name: "Micro Badger", link: "https://microbadger.com/" }),
(activemq:Technology { name: "ActiveMQ", link: "http://activemq.apache.org/" }),

// databases
(sql:Database { name: "SQL Server", link: "https://www.microsoft.com/en-us/sql-server/sql-server-2017" }),
(mongo:Database { name: "MongoDB", link: "https://www.mongodb.com/" }),
(neo:Database { name: "Neo4j", link: "https://neo4j.com/" }),
(kaha:Database { name: "KahaDB", link: "http://activemq.apache.org/kahadb.html" } ),

// tools
(vs:Tool { name: "Visual Studio", link: "https://www.visualstudio.com/vs/" }),
(vsc:Tool { name: "Visual Studio Code", link: "https://code.visualstudio.com/" }),
(aug:Tool { name: "Augury", link: "https://augury.angular.io/" }),
(rdt:Tool { name: "Redux DevTools", link: "https://github.com/zalmoxisus/redux-devtools-extension" }),
(ngsw:Tool { name: "ng swagger gen", link: "https://www.npmjs.com/package/ng-swagger-gen" }),
(compo:Tool { name: "compo doc", link: "https://compodoc.app/", categories: "docu" }),
(mt:Tool { name: "Marble Testing", link: "https://www.youtube.com/watch?v=dwDtMs4mN48", categories: "qa" } ),
(fx:Tool { name: "DocFX", link: "https://dotnet.github.io/docfx/", categories: "docu" }),
(ngcli:Tool { name: "Angular CLI", link: "https://cli.angular.io/" }),
(nx:Tool { name: "Nrwl nx", link: "https://nrwl.io/nx" }),
(ngcon:Tool { name: "Angular Console", link: "https://angularconsole.com/" }),
(sv3d:Tool { name: "SoftVis 3D", link: "https://softvis3d.com/" }),
(rsh:Tool { name: "Resharper", link: "https://www.jetbrains.com/resharper/" }),
(gt:Tool { name: "Git Tower", link: "https://www.git-tower.com/" }),
(ssc:Tool { name: "Sonar Scanner", link: "https://www.npmjs.com/package/sonar-scanner" }),
(sme:Tool { name: "Source Map Explorer", link: "https://www.npmjs.com/package/source-map-explorer" }),
(wjs:Tool { name: "Wallaby", link: "http://wallabyjs.com/", categories: "qa" }),
(lh:Tool { name: "Lighthouse", link: "https://developers.google.com/web/tools/lighthouse/", categories: "qa" }),
(gwt:Tool { name: "Google Webbmaster Tools", link: "https://www.google.com/webmasters/tools/" }),
(david:Tool { name: "David DM", link: "https://david-dm.org/" }),
(cvlt:Tool { name: "Coverlet", link: "https://github.com/tonerdo/coverlet" }),

// frontend frameworks
(ng:Framework { name: "Angular", link:"https://angular.io/", categories: "frontend" }),

// backend frameworks
(nc:Framework { name: ".NET Core", link:"https://dotnet.github.io/", categories: "backend" }),
(asp:Framework { name: "ASP.NET Core", link:"https://www.asp.net/core/overview" }),

// frontend libraries
(am:Library { name: "Angular Material", link: "https://material.angular.io/" }),
(rx:Library { name: "RxJS", link: "https://rxjs-dev.firebaseapp.com/", gitHub: "https://github.com/reactivex/rxjs" }),
(ngrx:Library { name: "ngrx", link: "http://ngrx.github.io/" }),
(hmr:Library { name: "Hammer", link: "https://hammerjs.github.io/" }),
(rvn:Library { name: "Raven", link: "https://github.com/getsentry/raven-js" }),
(stomp:Library { name: "ng2-stomp", link: "https://github.com/stomp-js/ng2-stompjs" } ),

// backend libraries
(sw:Library { name: "Swagger", link: "https://github.com/domaindrivendev/Swashbuckle.AspNetCore" }),
(bm:Library { name: "BenchmarkDotNet.Core", link: "https://benchmarkdotnet.org/" } ),
(ds:Library { name: "DevGem.Sentry", link: "https://gitlab.com/devgem/devgem-sentry" } ),
(efsql:Library { name: "EF SQLServer", link: "https://docs.microsoft.com/en-us/ef/core/" } ),
(efsqlite:Library { name: "EF SQLLite" } ),
(masstr:Library { name: "Masstransir", link: "http://masstransit-project.com/" } ),

// languages
(js:Language { name: "Javascript", link: "https://en.wikipedia.org/wiki/JavaScript" }),
(css:Language { name: "CSS", link: "https://nl.wikipedia.org/wiki/Cascading_Style_Sheets", specificityLnk: "https://specificity.keegan.st" }),
(sass:Language { name: "SASS", link: "https://sass-lang.com/" }),
(ts:Language { name: "Type Script", link: "https://www.typescriptlang.org/" }),
(html:Language { name: "HTML", link: "https://nl.wikipedia.org/wiki/HyperText_Markup_Language" }),
(cs:Language { name: "C#", link: "https://nl.wikipedia.org/wiki/C%E2%99%AF" }),
(yml:Language { name: "YAML", link: "http://yaml.org/" }),

// relations
(sq)-[:USES]->(sv3d),
(kb)-[:RUNS_ON]->(do),
(kb)-[:CONFIG_IN]->(yml),
(gl)-[:ALM]->(git),
(git)-[:MANAGED_WITH]->(gt),
(gl)-[:PACKAGES_TO]->(dk),
(gl)-[:DEPLOYS_WITH]->(glr),
(gl)-[:PACKAGES_WITH]->(glr),
(glr)-[:DEPLOYS_ON]->(kb),
(gl)-[:BUILDS_WITH]->(glr),
(glr)-[:RUNS_IN]->(dk),
(glr)-[:HOSTED_ON]->(do),
(glr)-[:CONFIG_IN]->(yml),
(dk)-[:STORED_IN]->(dh),
(dk)-[:ORCHESTRATED_WITH]->(kb),
(dk)-[:CONFIG_IN]->(yml),
(dk)-[:LAYERS]->(mbg),
(kb)-[:SECURED_BY]->(ruri),
(kb)-[:DDOS_MITIGATION]->(cf),
(nc)-[:SOURCECONTROL]->(git),
(ng)-[:SOURCECONTROL]->(git),

// frontend
(ng)-[:DEVELOPED_IN]->(vsc),
(vsc)-[:LIVE_TESTING]->(wjs),
(ng)-[:GENERATED_WITH]->(ngcli),
(ngcli)-[:ENTERPRISE_GRADE]->(nx),
(ngcon)-[:UI_FOR]->(ngcli),
(ng)-[:USES]->(ngsw),
(ng)-[:LOGGING]->(ga),
(ng)-[:USERS]->(insp),
(ng)-[:OPTIMIZE]->(gwt),
(ng)-[:DEPENDENCIES]->(david),
(david)-[:SOURCECONTROL]->(github),
(rvn)-[:ERRORS]->(se),
(ng)-[:RUNTIME_ANALYSIS]->(lh),
(rx)-[:OBSERVABLE_TESTING]->(mt),
(ngsw)-[:INSPECTS]->(sw),
(ngsw)-[:PRODUCES]->(ts),
(ng)-[:PROFILED_WITH]->(aug),
(ngrx)-[:DEBUGGED_WITH]->(rdt),
(ng)-[:DESIGN]->(am),
(ng)-[:STREAMS]->(rx),
(ng)-[:STATE]->(ngrx),
(ng)-[:GESTURES]->(hmr),
(ng)-[:USES]->(rvn),
(ng)-[:WRITTEN_IN]->(ts),
(ng)-[:WRITTEN_IN]->(sass),
(ng)-[:WRITTEN_IN]->(html),
(ts)-[:PRODUCES]->(js),
(sass)-[:PRODUCES]->(css),
(ng)-[:SSR]->(ngun),
(ng)-[:STATIC_ANALYSIS]->(ssc),
(ng)-[:INSPECT]->(sme),
(ssc)-[:REPORT]->(sq),
(ng)-[:DOCUMENTED_IN]->(compo),
(stomp)-[:CONNECTS_TO]->(activemq),
(ng)-[:USES]->(stomp),

// backend
(nc)-[:DEVELOPED_IN]->(vs),
(vs)-[:USES]->(rsh),
(nc)-[:WRITTEN_IN]->(cs),
(nc)-[:USES]->(asp),
(nc)-[:LOGGING]->(lda),
(nc)-[:PERFORMANCE]->(appi),
(asp)-[:USES]->(sw),
(nc)-[:USES]->(bm),
(nc)-[:USES]->(ds),
(ds)-[:ERRORS]->(se),
(nc)-[:STATIC_ANALYSIS]->(sq),
(nc)-[:USES]->(id4),
(nc)-[:RELATIONAL_DB]->(sql),
(nc)-[:DOCUMENT_DB]->(mongo),
(nc)-[:GRAPH_DB]->(neo),
(nc)-[:DOCUMENTED_IN]->(fx),
(nc)-[:COVERAGE]->(cvlt),
(nc)-[:ORM]->(efsql),
(nc)-[:ORM_TEST]->(efsqlite),
(nc)-[:MESSAGE_ABSTRACTION]->(masstr),
(masstr)-[:MESSAGING]->(activemq),
(activemq)-[:STORAGE]->(kaha)



=============================

// show all nodes
MATCH (n) RETURN (n)


