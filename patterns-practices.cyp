// Testing BDD specflow automated acceptance criteria 
// Evolutionary architecture
// Sociocracy 3.0
// hexagonal architecture - http://bit.ly/1GZuFW9
// Single responsibility principle - http://bit.ly/1zOFMxl - “Gather together those things that change for the same reason, and separate those things that change for different reasons.”

============================

// style
:style https://gitlab.com/devgem/techradar/raw/master/patterns-practices.grass

============================

// delete all nodes
MATCH (n)
DETACH DELETE n

=============================

// create all nodes
CREATE

// categories
(mgy:Category { name: "Methodology" }),
(sd:Category { name: "Software design" }),
(gd:Category { name: "Graphical design" }),
(sa:Category { name: "Software Architecture" }),
(pr:Category { name: "Principles" }),
(pa:Category { name: "Patterns" }),
(mkt:Category { name: "Marketing" }),

// techniques
(sm:Technique { name: "Story mapping", link: "https://jpattonassociates.com/user-story-mapping/" } ),
(es:Technique { name: "Event storming", link: "https://techbeacon.com/introduction-event-storming-easy-way-achieve-domain-driven-design" } ),

// methodologies
(ag:Item { name: "Agile", link: "http://agilemanifesto.org/" }),
(scrm:Item { name: "SCRUM", link: "https://www.scrum.org/" }),
(kb:Item { name: "Kanban", link: "https://leankit.com/learn/kanban/what-is-kanban/" }),
(sf:Item { name: "SAFE", link: "https://www.scaledagileframework.com/" }),
(f12:Item { name: "12 factor app", link: "https://12factor.net/" }),

// software design
(sip:Item { name: "SIP", link: "https://blog.strongbrew.io/the-sip-principle/" }),
(sb:Item { name: "Sandbox", link: "https://blog.strongbrew.io/A-scalable-angular2-architecture/. https://blog.strongbrew.io/A-scalable-angular-architecture-part2/" }),
(sdc:Item { name: "Smart dumb components", link: "https://medium.com/@dan_abramov/smart-and-dumb-components-7ca2f9a7c7d0" }),
(ioc:Item { name: "IoC", link: "https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-2.1" }),
(ddd:Item { name: "DDD", link: "https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/microservice-ddd-cqrs-patterns/ddd-oriented-microservice" }),
(bdd:Item { name: "BDD", link: "https://www.agilealliance.org/glossary/bdd/" }),
(tdd:Item { name: "TDD", link: "https://www.agilealliance.org/glossary/tdd/" }),

// principles
(kiss:Item { name: "KISS", link: "https://code.tutsplus.com/tutorials/3-key-software-principles-you-must-understand--net-25161" }),
(dry:Item { name: "DRY", link: "https://code.tutsplus.com/tutorials/3-key-software-principles-you-must-understand--net-25161" }),
(yagni:Item { name: "YAGNI", link: "https://code.tutsplus.com/tutorials/3-key-software-principles-you-must-understand--net-25161" }),
(solid:Item { name: "SOLID", link: "https://en.wikipedia.org/wiki/SOLID" }),

// software architecture
(c4:Item { name: "C4 model", link: "https://c4model.com/" }),
(se:Item { name: "Software engineering", link: "https://www.youtube.com/watch?v=lLnsi522LS8" }),
(st:Item { name: "Slices not layers", link: "https://www.youtube.com/watch?v=wTd-VcJCs_M" }),
(cui:Item { name: "Composable UI", link: "https://vimeo.com/223985688" }),
(ms:Item { name: "Microservices", link: "https://martinfowler.com/articles/microservices.html" }),
(ref2:Item { name: "Refactoring 2", link: "https://www.amazon.com/Refactoring-Improving-Existing-Addison-Wesley-Signature/dp/0134757599" }),

// patterns
(estore:Item { name: "Event store", link: "https://eventstore.org/" }),
(cqrs:Item { name: "CQ(R)S", link: "https://martinfowler.com/bliki/CQRS.html" }),

// graphical design
(ad:Item { name: "Atomic design", link: "http://bradfrost.com/blog/post/atomic-web-design/" }),
(fl:Item { name: "Fitt's law", link: "https://en.wikipedia.org/wiki/Fitts%27s_law" } ),
(gr:Item { name: "Golden ratio", link: "https://en.wikipedia.org/wiki/Golden_ratio" } ),
(cw:Item { name: "Color wheel", link: "https://color.adobe.com/create/color-wheel/" } ),

// marketing
(si:Tool { name: "Smart Insights", link: "https://www.smartinsights.com/" }),
(rc:Item { name: "RACE", link: "https://www.smartinsights.com/digital-marketing-strategy/race-a-practical-framework-to-improve-your-digital-marketing/" }),
(stc:Item { name: "SOSTAC", link: "https://www.smartinsights.com/digital-marketing-strategy/sostac-model/" } ),

// marketing relations
(rc)-[:IS]->(mkt),
(rc)-[:RESOURCE]->(si),
(mkt)-[:PLAN]->(stc),

// methodology relations
(ag)-[:IS]->(mgy),
(scrm)-[:IS]->(ag),
(scrm)-[:USES]->(sm),
(kb)-[:IS]->(mgy),
(ag)-[:CAN_USE]->(kb),
(scrm)-[:TO_SCALE]->(sf),
(f12)-[:IS]->(mgy),

// software design relations
(sip)-[:IS]->(sd),
(sb)-[:IS]->(sd),
(sdc)-[:IS]->(sd),
(ioc)-[:IS]->(sd),
(ddd)-[:IS]->(sd),
(tdd)-[:IS]->(sd),
(bdd)-[:IS]->(sd),
(ddd)-[:USES]->(es),

// principles relations
(kiss)-[:IS]->(pr),
(kiss)-[:PREFER_OVER]->(dry),
(dry)-[:IS]->(pr),
(yagni)-[:IS]->(pr),
(solid)-[:IS]->(pr),

// software architecture relations
(c4)-[:IS]->(sa),
(se)-[:IS]->(sa),
(st)-[:IS]->(sa),
(cui)-[:IS]->(sa),
(ms)-[:IS]->(sa),
(ref2)-[:IS]->(sa),

// patterns relations
(estore)-[:IS]->(pa),
(cqrs)-[:IS]->(pa),

// graphic design relations
(ad)-[:IS]->(gd),
(fl)-[:IS]->(gd),
(gr)-[:IS]->(gd),
(cw)-[:IS]->(gd)

=============================

// show all nodes
MATCH (n) RETURN (n)
